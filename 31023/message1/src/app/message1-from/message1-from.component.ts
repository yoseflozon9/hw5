import { Component, OnInit , Output , EventEmitter } from '@angular/core';
import {FormGroup , FormControl} from '@angular/forms';
import { Message1Service } from '../message1/message1.service';

 @Component({
  selector: 'message1From',
 templateUrl: './message1-from.component.html',
  styleUrls: ['./message1-from.component.css']
})
export class Message1FormsComponent implements OnInit {
  //בניית מבנה נתונים בקוד שהוא מתאים אחד לאחד בטופס

 service:Message1Service;
 //בניית ערוץ התקשורת בין message form to message
   @Output() addMessage:EventEmitter<any> = new EventEmitter<any>(); //any מגדיר סוג מסוים של מידע, במקרה הזה כל סוג
 
  @Output() addMessagePs:EventEmitter<any> = new EventEmitter<any>(); //אירוע פסימי
  //Reactive Form
  msgform = new FormGroup({
    message:new FormControl(),
    user:new FormControl()
  });

  /*שליחת הנתונים
  sendData() {
    console.log(this.msgform.value);
    this.service.postMessage(this.msgform.value).subscribe(
      response => {
        console.log(response.json())
      }
    );
  }

 constructor(service: Message1Service) { 
    this.service = service;
  }*/
 //שליחת הנתונים
 
 sendData() {
    //הפליטה של העדכון לאב
  this.addMessage.emit(this.msgform.value.message);

   console.log(this.msgform.value);
   this.service.postMessage(this.msgform.value).subscribe(
     response => {

      console.log(response.json());
      this.addMessagePs.emit();
     }
   );
 }

 constructor(service: Message1Service) { 
   this.service = service;
 }

 ngOnInit() {
  }

}
