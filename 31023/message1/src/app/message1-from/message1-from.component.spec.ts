import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Message1FormsComponent } from './message1-from.component';

+describe('MessagesFormsComponent', () => {
  let component: Message1FormsComponent;
 let fixture: ComponentFixture<Message1FormsComponent>;

 beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Message1FormsComponent ]
    })
   .compileComponents();
 }));

 beforeEach(() => {
   fixture = TestBed.createComponent(Message1FormsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
  expect(component).toBeTruthy();
  });

});
