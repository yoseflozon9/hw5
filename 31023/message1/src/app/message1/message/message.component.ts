import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { Message1Service } from '../message1.service';
@Component({
  selector: 'message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {
message;
  constructor(private route:ActivatedRoute, private service :Message1Service){}

  ngOnInit() {
    this.route.paramMap.subscribe(params=>{
 let id = params.get ('id');
 this.service.getMessage(id).subscribe(response=>{
   this.message=response.json();
 })
    })
  }

}
