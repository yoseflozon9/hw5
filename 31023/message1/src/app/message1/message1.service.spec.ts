import { TestBed, inject } from '@angular/core/testing';
import { Message1Service } from './message1.service';

describe('Message1Service', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [Message1Service]
    });
  });

  it('should be created', inject([Message1Service], (service: Message1Service) => {
    expect(service).toBeTruthy();
  }));
});
