import { Component, OnInit } from '@angular/core';
import { Response } from '@angular/http/src/static_response';
import { Message1Service } from './message1.service';
@Component({
  selector: 'message1',
  templateUrl: './message1.component.html',
  styleUrls: ['./message1.component.css']
})
export class Message1Component implements OnInit {
messages;
messagesKeys = [];

  constructor(private service:Message1Service) { 
    service.getMessages().subscribe(Response=>{
      this.messages=Response.json();
      this.messagesKeys=Object.keys(this.messages);

    });
  }
//הפונקציה שמוסיפה את המידע לטבלה באופן אופטימיסטיק
  optimisticAdd(message){
      //console.log("addMessage work "+message); בדיקה
     var newKey = parseInt(this.messagesKeys[this.messagesKeys.length - 1],0) + 1;
    var newMessageObject = {};
      newMessageObject['body'] = message; //גוף ההודעה עצמה
      this.messages[newKey] = newMessageObject;
      this.messagesKeys = Object.keys(this.messages);
    }
  
  //תזמון
    pessimisticAdd(){
      this.service.getMessages().subscribe(
        response=>{
          this.messages = response.json();
          this.messagesKeys = Object.keys(this.messages);
      });
    }
  
  //מחיקת רשומה
    deleteMessage(key){
      console.log.apply(key);
      let index = this.messagesKeys.indexOf(key);
      this.messagesKeys.splice(index,1); //אחד מבטא מחיקת רשומה אחת
  
  
      //delete from server
      this.service.deleteMessage(key).subscribe(
        response=>console.log(response)
      );
    }
  ngOnInit() {
  }

}

