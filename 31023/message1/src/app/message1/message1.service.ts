import { Injectable } from '@angular/core';
import { Http , Headers } from '@angular/http';
import { HttpParams } from '@angular/common/http';
@Injectable()
export class Message1Service {

  //שמאל תכונה, ימין סוג המחלקה

 http:Http; //המחלקה מנהלת את התקשורת עם השרת, ספציפי עם הרסט איפיאי

  getMessages(){
    //return ['Message 1','Message 2' ,'Message 3' ,'Message 4'];
  // return ['Message 1','Message 2' ,'Message 3' ,'Message 4'];
 //get messages from the SLIM rest API (No DB)

  return this.http.get('localhost/31023/slim2/messages');
  }
  getMessage(id){
    return this.http.get('localhost/31023/slim2/messages/'+id);
  }
  postMessage(data){
        //המרת גייסון למפתח וערך
        let options = {
          headers: new Headers({
            'content-type':'application/x-www-form-urlencoded'
          })
        }
    
        let params = new HttpParams().append('message',data.message);
       
     return this.http.post('localhost/31023/slim2/messages', params.toString(), options);
    
      }
      deleteMessage(key){
           return this.http.delete('localhost/31023/slim2/messages'+ key);
         }
  //בתוך הקונסטרקטור נוצר מופע של האובייקט, דפנדיסי אינג'קטיון
  constructor(http:Http) { 
    this.http = http;
  }

}

