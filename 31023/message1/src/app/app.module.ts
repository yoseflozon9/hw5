import { BrowserModule } from '@angular/platform-browser';
import {HttpModule} from '@angular/http'
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { Message1Component } from './message1/message1.component';
import { Message1Service } from './message1/message1.service';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Message1FormsComponent } from './message1-from/message1-from.component';
import { NavigationComponent } from './navigation/navigation.component';
import {RouterModule} from '@angular/router';
import { UsersComponent } from './users/users.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { MessageComponent } from './message1/message/message.component';
@NgModule({
  declarations: [
    AppComponent,
    Message1Component,
    Message1FormsComponent,
    NavigationComponent,
    UsersComponent,
    NotFoundComponent,
    MessageComponent
    
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      {path:'',component:Message1Component},
      {path:'users',component:UsersComponent},
      {path:'message/:id',component:Message1Component},
      {path:'**',component:NotFoundComponent},
    ])
  ],
  providers: [
    Message1Service
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
