<?php
//require "vendor/autoload.php"
require "bootstrap.php";
use Chatter\Models\Message;
use Chatter\Models\User;
$app = new \Slim\App();
$app->get('/hello/{name}', function($request, $response,$args){
   return $response->write('Hello '.$args['name']);
});
/*חייב לתת 3 פרמטרים, שאלה תשובה, ומה אני דורש*/

$app->get('/customers/{id}', function($request, $response, $array){
    /*אני דורש את הID מהמשתמש*/
    $id = $request->getAttribute('id');
    $json = '{"1":"John", "2":"Jack","3":"Shelly","4":"Amir"}';
    /*הכרזה על המערך*/
    $array = json_decode($json, true);
/*אנחנו שואלים אם זה מופיע במערך*/
    if(array_key_exists($id, $array)){
        $name = $array[$id];
        /*אני מחזיר למשתמש*/
        return $response->write('Hello, '.$name);
    }
    else{
        return $response->write('Hello, id does not exist');
    }
});
//שליפת נתונים
$app->get('/messages', function($request, $response,$args){
    $_message = new Message();
    $messages = $_message->all();
//מערך ריק
    $payload = [];
    //לולאה על כל הנתונים
    foreach($messages as $u){
        //הכנסת הנתונים מהטבלה לפי הID
        $payload[$u->id] = [
            "body" => $u->body
        ];
    }
    return $response->withStatus(200)->withJson($payload);
});

//*$app->get('/messages/{id}', function($request, $response,$args){
    //$_id-$args['id'];
    //$message=Message::find($_id);

   // return $response->withStatus(200)->withJson($message);
//});


//יצירת אובייקט חדש לDB
$app->post('/messages', function($request, $response,$args){
    $message_id = $request->getParsedBodyParam('user_id','');
    $body = $request->getParsedBodyParam('body','');
    $_message = new Message();
    $_message->user_id = $message_id;
    $_message->body = $body;
    $_message->save();
       
    if($_message->id){
        $payload = ['message_id' => $_message->id];
        return $response->withStatus(201)->withJson($payload);
    }
    else{
        return $response->withStatus(400);
    }
});


$app->delete('/messages/{id}', function($request, $response,$args){
    $message = Message::find($args['id']);
    $message->delete();
    
    if($message->exists){
        return $response->withStatus(400);
    }
    else{
       return $response->withStatus(200);
   }
});
/*
//הוספת קבוצה
$app->post('/users/bulk', function($request, $response, $args){
    $payload = $request->getParsedBody();
        
    User::insert($payload);
    return $response->withStatus(201)->withJson($payload);*/
//
    $app->get('/users', function($request, $response,$args){
        $_user = new User();
        $users = $_user->all();
    
        $payload = [];
        foreach($users as $u){
            $payload[$u->id] = [
                "username" => $u->username,
                "email" => $u->email
            ];
        }
        return $response->withStatus(200)->withJson($payload);
    });
    
    //יצירה
    $app->post('/users', function($request, $response,$args){
        $email = $request->getParsedBodyParam('email','');
        $username = $request->getParsedBodyParam('username','');
        $_user = new User();
        $_user->email = $email;
        $_user->username = $username;
        $_user->save();
           
        if($_user->id){
            $payload = ['user_id' => $_user->id];
            return $response->withStatus(201)->withJson($payload);
        }
        else{
            return $response->withStatus(400);
        }
    });
    
    //מחיקה
    $app->delete('/users/{user_id}', function($request, $response,$args){
        $user = User::find($args['user_id']);
        $user->delete();
        
        if($user->exists){
            return $response->withStatus(400);
        }
        else{
           return $response->withStatus(200);
       }
    });
    
    //הוספת קבוצה
    $app->post('/users/bulk', function($request, $response, $args){
        $payload = $request->getParsedBody();
            
        User::insert($payload);
        return $response->withStatus(201)->withJson($payload);
    });

    $app->options('/{routes:.+}', function ($request, $response, $args) {
        return $response;
    });
    //jwt
    $app->post('/auth', function($request, $response, $args){
        $user = $request->getParsedBodyParam('user','');
        $password = $request->getParsedBodyParam('password','');
// we need to go to DB' but not now!
        if  ($user=='jack' && $password=='1234'){
            //create jwt and send to the cliant.
            $payload=['token'=>'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpUIn0.eyJzdWIiOiIxMjM0NTY3IiwibmFtZSI6ImphY2siLCJhZG1pbiI6dHJ1ZX0.Yrl711ysJkPakpmDMdUCV2ajFnZbuNS98PXKpZYeBHU'];
           return $response->withStatus(200)->withJson($payload);
        } 
        else {
            $payload=['token'=> null];
            return $response->withStatus(403)->withJson($payload);
        }
       
    });
    $app->add(new \Slim\Middleware\JwtAuthentication([
        "secret" => "supersecret",
        "path" => ['/messsages']
    ]));

    
    
    $app->add(function ($req, $res, $next) {
        $response = $next($req, $res);
        return $response
                ->withHeader('Access-Control-Allow-Origin', '*')
                ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
                ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    });


    //חייב להיות בסוף מריץ את הפונקציות.
$app->run();