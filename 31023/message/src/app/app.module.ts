import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { MessagesComponent } from './messages/messages.component';
import { MessageService } from './message.service';
import { MessageFormsComponent } from './message-forms/message-forms.component';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { NavigationComponent } from './navigation/navigation.component';
import { NotFuondComponent } from './not-fuond/not-fuond.component';
import { UserComponent } from './user/user.component';
import { RouterModule } from '@angular/router';
import { Message1Component } from './messages/message1/message1.component';
import { LoginComponent } from './login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    MessagesComponent,
    MessageFormsComponent,
    NavigationComponent,
    NotFuondComponent,
    UserComponent,
   
    Message1Component,
   
    LoginComponent,
  ],
  imports: [
    BrowserModule,
    BrowserModule,
      HttpModule,
      FormsModule,
     ReactiveFormsModule,
     RouterModule.forRoot([
          {path: '', component: MessagesComponent},
          {path: 'users', component: UserComponent},
          {path: 'login', component: LoginComponent},
          {path: 'message/:id', component: Message1Component},
          {pathMatch: 'full',path: 'message-form/:id', component: MessageFormsComponent},
          {path: '**', component: NotFuondComponent}
     
        ])
     ],
    providers: [
       MessageService,
       
       ],
  bootstrap: [AppComponent]
})
export class AppModule { }
