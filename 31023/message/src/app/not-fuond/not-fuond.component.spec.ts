import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotFuondComponent } from './not-fuond.component';

describe('NotFuondComponent', () => {
  let component: NotFuondComponent;
  let fixture: ComponentFixture<NotFuondComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotFuondComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotFuondComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
