import { Injectable } from '@angular/core';
import { Http , Headers } from '@angular/http';
import { HttpParams } from '@angular/common/http';
import 'rxjs/Rx';
 @Injectable()
 export class MessageService {
 
   //שמאל תכונה, ימין סוג המחלקה
   http:Http; //המחלקה מנהלת את התקשורת עם השרת, ספציפי עם הרסט איפיאי
 
   getMessages(){
   // return ['Message 1','Message 2' ,'Message 3' ,'Message 4'];
   // return ['Message 1','Message 2' ,'Message 3' ,'Message 4'];
   let token = localStorage.getItem('tokem');
   let options = {
     headers: new Headers ({
       'Authorization':'Bearer '+token
     })
   }
   return this.http.get('http://localhost/31023/slim2/messages',options);
   

    }
    deleteMessage(key){
          return this.http.delete('http://localhost/31023/slim2/messages/'+ key);
      
       }
    login(credentials){
      let options = {
        headers: new Headers({
          'content-type':'application/x-www-form-urlencoded'
        })
      }
      let params = new HttpParams().append('user',credentials.user).append('password',credentials.password);
      return this.http.post('http://localhost/31023/slim2/auth',params.toString(),options).map(respons=>
      {let token = respons.json().token;
        if(token) localStorage.setItem('token', token);
        });    
       }
 
 postMessage(data){
   //המרת גייסון למפתח וערך
   let options = {
     headers: new Headers({
       'content-type':'application/x-www-form-urlencoded'
   })
  }
   let params = new HttpParams().append('message',data.message);
   
  return this.http.post('http://localhost/31023/slim2/messages', params.toString(), options);
 }
//הודעה בודדת
 getMessage(id){
      return this.http.get('http://localhost/31023/slim2/messages/'+ id);
   }

   //בתוך הקונסטרקטור נוצר מופע של האובייקט, דפנדיסי אינג'קטיון
   constructor(http:Http) { 
     this.http = http;
    }
    
    }