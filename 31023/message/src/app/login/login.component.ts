import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MessageService } from '../message.service';
import { Router } from '@angular/router';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  invalid=false;
loginform = new FormGroup({
user:new FormControl(),
password:new FormControl(),
 });
 
 login(){
this.service.login(this.loginform.value).subscribe(response=>{
this.router.navigate(['/']);
  },error=>{this.invalid=true;}
  )
}
logout(){
  localStorage.removeItem('token');
  //this.invalid=false;
}

  constructor(private service:MessageService, private router:Router){}

  ngOnInit() {
  }

}
