
import { MessageService } from './../message.service';
import { Component, OnInit  , Output , EventEmitter  } from '@angular/core';
import {FormGroup , FormControl} from '@angular/forms';
@Component({
  selector: 'message-forms',
  templateUrl: './message-forms.component.html',
  styleUrls: ['./message-forms.component.css']
})
export class MessageFormsComponent implements OnInit {
   //בניית ערוץ התקשורת בין message form to message
 @Output() addMessage:EventEmitter<any> = new EventEmitter<any>(); //any מגדיר סוג מסוים של מידע, במקרה הזה כל סוג

 @Output() addMessagePs:EventEmitter<any> = new EventEmitter<any>(); //אירוע פסימי
  service:MessageService;
  msgform = new FormGroup({
    message:new FormControl(),
 
     user:new FormControl()
  });
  sendData() {
    //הפליטה של העדכון לאב

  this.addMessage.emit(this.msgform.value.message);
    console.log(this.msgform.value);
    this.service.postMessage(this.msgform.value).subscribe(
      response => {
        console.log(response.json())
        this.addMessagePs.emit();
     }
   );
 }
 constructor(service:MessageService) { 
  this.service = service;
}

  ngOnInit() {
  }

}



