import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessageFormsComponent } from './message-forms.component';

describe('MessageFormsComponent', () => {
  let component: MessageFormsComponent;
  let fixture: ComponentFixture<MessageFormsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessageFormsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessageFormsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
