import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MessageService } from '../../message.service';
@Component({
  selector: 'message1',
  templateUrl: './message1.component.html',
  styleUrls: ['./message1.component.css']
})
export class Message1Component implements OnInit {

  message;
  
   //מוגדר רק כאשר מבצעים dependacy injection
    //גורם לאנגולר לייצר אובייקט חדש וליצור תכונה של אותו אובייקט
    constructor(private route: ActivatedRoute , private service:MessageService) { }
  
    //קוד שפועל כאשר הקומפוננט נוצר
   ngOnInit() {
      this.route.paramMap.subscribe(params=>{
        let id = params.get('id');
        console.log(id);
        this.service.getMessage(id).subscribe(response=>{
         this.message = response.json();
          console.log(this.message);
 
      })
     })
  }

}

